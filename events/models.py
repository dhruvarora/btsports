from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

now = timezone.now()

class Event(models.Model):
    date_added = models.DateField(default=now)
    title = models.CharField(max_length=150)
    introImage = models.FileField(upload_to="events/intro-images", max_length=150)
    introText = models.CharField(max_length=35)
    fullText = RichTextField(blank=True, null=True)
    fullImage = models.FileField(max_length=150, upload_to="events/full-images")
    meta_desc = models.CharField(max_length=50, blank=True, null=True)
    meta_keywords = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.title
