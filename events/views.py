from django.shortcuts import render
from .models import Event

def index(request):
    allPosts = Event.objects.all()

    return render(request, "blog/index.html", {
    "allposts":allPosts,
    })


def EventView(request, id, slug):
    post = Event.objects.get(pk=id)

    return render(request, "events/event.html", {
    "post":post,
    })
