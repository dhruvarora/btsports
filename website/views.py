from django.shortcuts import render
from .models import Querys, HomeSliders
from blog.models import Post
from events.models import Event

def index(request):
    blogs = Post.objects.all()[:3]
    events = Event.objects.all()[:3]
    sliders = HomeSliders.objects.all()

    return render(request, "index.html", {
    "blogs":blogs, "events":events, "sliders":sliders,
    })

def contact(request):
    if request.method == "POST":
        fname = request.POST.get("fname")
        lname = request.POST.get("lname")
        name = fname + lname
        email = request.POST.get("email")
        subject = request.POST.get("subject")
        message = request.POST.get("message")
        selfEmail = request.POST.get("copy")

        try:
            q = Querys(name=name, email=email, subject=subject, message=message)
            q.save()
        except:
            return render(request, "contact.html", {
            "message":"failed",
            })
        else:
            return render(request, "contact.html", {
            "message": "received",
            })
    else:
        return render(request, "contact.html", {

        })

def about(request):
    return render(request, "about.html", {})

def services(request):
    return render(request, "service.html", {})
