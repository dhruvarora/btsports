from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

now = timezone.now()

class Querys(models.Model):
    date = models.DateTimeField(default=now)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    subject = models.CharField(max_length=150)
    message = models.TextField()

    def __str__(self):
        return self.name

class HomeSliders(models.Model):
    title = models.CharField(max_length=150, blank=True, null=True)
    text = models.TextField(blank=True, null=True)
    image = models.FileField(max_length=150, upload_to="home-sliders/")

    def __str__(self):
        return self.title


class Testimonial(models.Model):
    date_added = models.DateField(default=now)
    title = models.CharField(max_length=150)
    author_name = models.CharField(max_length=50)
    testimonial = models.TextField(blank=True, null=True)
    published = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No")
    ))
    homepage = models.BooleanField(default=False, choices=(
        (True, "Yes"),
        (False, "No")
    ))

    def __str__(self):
        return self.title + " written by: " + self.author_name
