from django.contrib import admin

from .models import Querys, HomeSliders, Testimonial

admin.site.register(Querys)
admin.site.register(HomeSliders)
admin.site.register(Testimonial)
