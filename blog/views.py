from django.shortcuts import render

from .models import Post

def index(request):
    allPosts = Post.objects.all()

    return render(request, "blog/index.html", {
    "allposts":allPosts,
    })


def PostView(request, id, slug):
    post = Post.objects.get(pk=id)

    return render(request, "blog/post.html", {
    "post":post,
    })
