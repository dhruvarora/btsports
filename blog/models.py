from __future__ import unicode_literals

from django.db import models
from django.utils import timezone
from ckeditor.fields import RichTextField

now = timezone.now()

class Post(models.Model):
    date_added = models.DateTimeField(default=now)
    title = models.CharField(max_length=150)
    author = models.CharField(max_length=150)
    introText = RichTextField()
    introImage = models.FileField(max_length=150, upload_to="blog/intro-images")
    mainImage = models.FileField(max_length=150, upload_to="blog/main-images", blank=True, null=True)
    fullText = RichTextField()

    def __str__(self):
        return self.title
