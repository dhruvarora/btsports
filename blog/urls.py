from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^(?P<id>\d+)/(?P<slug>.+)/$', views.PostView, name="post-view"),
]
