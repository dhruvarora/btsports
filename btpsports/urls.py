from django.conf.urls import url, include
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include("website.urls", namespace="website")),
    url(r'^blog/', include("blog.urls", namespace="blog")),
    url(r'^photologue/', include('photologue.urls', namespace='photologue')),
    url(r'^events/', include("events.urls", namespace="events")),
]
